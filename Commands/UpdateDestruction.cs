﻿using Eco.Gameplay.Players;
using Eco.Gameplay.Systems.Chat;
using EcoColorLib;
using System;

namespace LaserControlLight.Commands
{
    class LaserLightCommand : IChatCommandHandler
    {
        [ChatCommand("lasercontrol", "Laser light basic command", ChatAuthorizationLevel.User)]
        public static void updatedestruction(User user, String argsString = "")
        {
            user.Player.SendTemporaryMessage(LaserControlLight.coloredPrefix + ChatFormat.UnderLine.Value + ChatFormat.Bold.Value + ChatFormat.Green.Value + "Laser Control Light");
            user.Player.SendTemporaryMessage(LaserControlLight.coloredPrefix + ChatFormat.Bold.Value+"Dev by quequierebego / quequiere");
            user.Player.SendTemporaryMessage(LaserControlLight.coloredPrefix + ChatFormat.Bold.Value + ChatFormat.Yellow.Value+ "For eco-serveur.fr");
            user.Player.SendTemporaryMessage(LaserControlLight.coloredPrefix + ChatFormat.Bold.Value + ChatFormat.Blue.Value + "Contact: https://discord.gg/auamCAg");
        }






    }
}
