﻿using Eco.Core.Plugins.Interfaces;
using EcoColorLib;
using LaserControlLight.Config;
using LaserControlLight.Thread;
using System;

namespace LaserControlLight
{

    public class LaserControlLight : IModKitPlugin, IServerPlugin
    {
        public static String prefix = "LaserControl: ";
        public static String coloredPrefix = ChatFormat.Green.Value + ChatFormat.Bold.Value + prefix + ChatFormat.Clear.Value;
        public static LaserLightConfig config;

        private Boolean started = false;

        public string GetStatus()
        {

            if (!started)
            {
                this.start();
                started = true;
            }

            return "";
        }


        public void start()
        {
            Console.WriteLine(LaserControlLight.prefix + " starting !");


            config = new LaserLightConfig("LaserControlLight", "config");
            if (config.exist())
            {
                config = config.reload<LaserLightConfig>();
            }
            else
            {
                config.save();
            }



            System.Threading.Thread objw = new System.Threading.Thread(() => WorldObjAnalyser.OjbectModifier());
            objw.Start();
        }
    }

}


