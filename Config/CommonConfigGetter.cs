﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaserControlLight.Config
{
    public class CommonConfigGetter
    {
        public virtual int getEnergyNeededForLaser()
        {
            return LaserControlLight.config.energyBaseNeededForLaser;
        }

        public virtual int getLaserNeeded()
        {
            return LaserControlLight.config.laserBaseNeeded;
        }
    }
}
