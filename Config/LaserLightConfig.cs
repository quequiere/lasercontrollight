﻿using JsonConfigSaver;
using Newtonsoft.Json;
using System;

namespace LaserControlLight.Config
{

    //check if dll loaded et lancer sur laserControl au besoin

    public class LaserLightConfig : JsonEcoConfig
    {

        [JsonProperty]
        public int secondsToDestroyMeteor = 60;


        [JsonProperty]
        public int energyBaseNeededForLaser = 10000;
        [JsonProperty]
        public int laserBaseNeeded = 5;


       // Type myType = Type.GetType("MyNamespace.MyClass");
        [JsonProperty]
        public int onlinePlayersNeededForLaser = 1;


        public static CommonConfigGetter commonGetter = null;
        

        public LaserLightConfig(string pluginName, string name) : base(pluginName, name)
        {
            
            if (commonGetter!=null || ! (commonGetter is CommonConfigGetter) && commonGetter!=null)
            {
                Console.WriteLine(LaserControlLight.prefix + "Cancel one configoverride");
            }
            else
            {
                commonGetter=new CommonConfigGetter();
                Console.WriteLine(LaserControlLight.prefix + "Light config controller registred !");
            }
             
        }

        public int getEnergyNeededForLaser()
        {
            return commonGetter.getEnergyNeededForLaser();
        }

        public int getLaserNeeded()
        {
            return commonGetter.getLaserNeeded();
        }

    }
}
